package GUI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.io.IOException;
import java.text.ParseException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.plaf.synth.SynthLookAndFeel;

import customWidgets.Container;
import customWidgets.JListCreator;
import customWidgets.JListFunctionality;
import customWidgets.JListStyle;


public class GUI extends JFrame {

	public GUI() throws IOException {
		initiateLookAndFeel();
		this.setTitle("GUI");
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		addWidgets();
		setLocationRelativeTo(null);
		this.setPreferredSize(new Dimension(900,580));
		this.setResizable(false);
		pack();
	}
	
	private void addWidgets() throws IOException {
				
		//setting the pane and its layout
		Container mainPane = new Container();
		this.getContentPane().setLayout(new BorderLayout());
		this.add(mainPane,BorderLayout.CENTER);
		mainPane.setLayout(new BorderLayout());
		
		//creating a layout object to be used by multiple panels
		GridBagLayout gbl = new GridBagLayout();
		
		//creating the news list panel, the custom list and setting the panel's layout
		JPanel listPanel = new JPanel();
		listPanel.setLayout(gbl);
		JListCreator jListPopulator = new JListCreator();
		JList list = new JList(jListPopulator.getJListItems().toArray());
	    list.setOpaque(false);
	    list.setForeground(new Color(255,255,255));
	    list.setCellRenderer(new JListStyle.CustomCellRenderer());
	    JScrollPane listScrollPane = new JScrollPane(list) {
	    	public void paintComponent(Graphics g) {
	    		g.setColor(getBackground());
	    		g.fillRect(0, 0, getWidth(), getHeight());
	    		super.paintComponent(g);
	    	}
	    };
	    listScrollPane.getViewport().setOpaque(false);
	    listScrollPane.setBackground(new Color(55,55,55,60));
		
		//creating the panel which will contain the news content and setting the layout
		//here we also create some widgets that will be displayed in the news content panel
		JPanel newsContentPanel = new JPanel();
		newsContentPanel.setLayout(gbl);
		JTextPane newsTextPane = new JTextPane();
		//newsTextPane.setContentType("text/html");
	    new JListFunctionality(list, newsTextPane);
	    JScrollPane newsContentScrollPane = new JScrollPane(newsTextPane) {
	    	public void paintComponent(Graphics g) {
	    		g.setColor(getBackground());
	    		g.fillRect(0, 0, getWidth(), getHeight());
	    		super.paintComponent(g);
	    	}
	    };
	    newsContentScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
	    newsContentScrollPane.setBackground(new Color(55,55,55,60));
		JButton btnExit = new JButton("Exit");

		ButtonHandler listener = new ButtonHandler(this);
		btnExit.addActionListener(listener);
		
		//creating and setting a rounded border for JTextPane
		newsContentScrollPane.getViewport().setOpaque(false);
		newsTextPane.setMargin(new Insets(20,20,20,20));
		
		//Finally, adding the list panel and news content panel to the pane
		mainPane.add(listPanel, BorderLayout.WEST);
		mainPane.add(newsContentPanel,BorderLayout.CENTER);

	    //creating a GridBagConstraint object
		GridBagConstraints c1 = new GridBagConstraints();
	
		//constraints for adding the custom list
		c1.fill = GridBagConstraints.VERTICAL;
		c1.weighty = 0.5;
		c1.weightx = 0;
		c1.gridx = 0;
		c1.gridy = 0;
		listPanel.add(listScrollPane, c1);
		
		c1.fill = GridBagConstraints.BOTH;
		c1.weighty = 0.5;
		c1.weightx = 0.5;
		c1.gridx = 0;
		c1.gridy = 0;
		c1.insets = new Insets(30,50,10,50);
		newsContentPanel.add(newsContentScrollPane, c1);
		
		c1.fill = GridBagConstraints.VERTICAL;
		c1.gridx = 0;
		c1.gridy = 1;
		c1.weightx = 0;
		c1.weighty = 0;
		c1.anchor = GridBagConstraints.LAST_LINE_END;
		c1.insets = new Insets(10,10,10,10);
		newsContentPanel.add(btnExit, c1);
	}

	public void initiateLookAndFeel() {
		SynthLookAndFeel lookAndFeel = new SynthLookAndFeel();
		try {
			lookAndFeel.load(GUI.class.getResourceAsStream("lookandfeel.xml"),GUI.class);
			UIManager.setLookAndFeel(lookAndFeel);
		}
		catch (ParseException e) {
			System.err.println("Look and Feel could not be loaded"+ lookAndFeel);
			e.printStackTrace();
		}
		catch (UnsupportedLookAndFeelException e) {
			System.err.println("Look and Feel not supported");
			e.printStackTrace();
		}
	}
}
