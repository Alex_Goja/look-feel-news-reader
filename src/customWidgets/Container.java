package customWidgets;

import java.awt.Graphics;
import java.awt.Image;
import java.io.IOException;
import javax.swing.JComponent;

public class Container extends JComponent{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Image image;
		
	public Container() throws IOException {
		this.image = ImageLoader.loadImage("./src/images/FrameBackground.png");	
	}
	
	public void paintComponent(Graphics g) {
		g.drawImage(image, 0, 0, null);
	}
}

