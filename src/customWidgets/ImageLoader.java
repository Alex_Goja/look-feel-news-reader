package customWidgets;

import java.awt.Image;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class ImageLoader {

	public static Image loadImage(String path) throws IOException {
		try {
			Image image = ImageIO.read(new File(path));
			return image;
		}
		catch(IOException e) {
			new IOException("Could not open the file. Please make sure that the " +
					"specified path " + path +" is correct");
			e.printStackTrace();
		}
		return null;	
	}
}