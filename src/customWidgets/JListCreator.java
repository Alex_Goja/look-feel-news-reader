package customWidgets;

import java.io.IOException;
import java.util.ArrayList;

public class JListCreator {

	private static ArrayList<ListItem> itemsArr = new ArrayList<ListItem>();
	private String[] paths = new String[] {"./src/images/BBC.png", "./src/images/METRO.png", "./src/images/CNBC.png",
			"./src/images/REUTERS.png", "./src/images/SKY.png"};

	public JListCreator() {
		createList();
	}
	
	private void createList() {
		for (int i=0; i<paths.length; i++) {
			ListItem item = new ListItem();
			int start = paths[i].lastIndexOf("/");
			int end = paths[i].lastIndexOf(".");
			try {
				item.setImage(ImageLoader.loadImage(paths[i]));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			item.setName("  "+paths[i].substring(start+1, end)+" News");	
			itemsArr.add(item);
		}
	}
	
	public ArrayList<ListItem> getJListItems() {
		return itemsArr;
	}
}