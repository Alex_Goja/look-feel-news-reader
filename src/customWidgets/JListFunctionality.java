package customWidgets;

import java.io.IOException;
import java.net.MalformedURLException;

import javax.swing.JList;
import javax.swing.JTextPane;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import news_content.NewsContentExtractor;

public class JListFunctionality implements ListSelectionListener {

	private JList list;
	private JTextPane contentPane;
	
	public JListFunctionality(JList list, JTextPane contentPane) {
		this.list = list;
		this.contentPane = contentPane;
		list.addListSelectionListener(this);
	}

	@Override
	public void valueChanged(final ListSelectionEvent e) {	
		new Thread() {
			public void run() {
				//e.getValueAdjusting()==true prevents listener to be invoked twice
				if (e.getValueIsAdjusting()==true) {
					switch(list.getSelectedIndex()) {
					case 0: getContent("http://feeds.bbci.co.uk/news/rss.xml");
					case 1: getContent("http://metro.co.uk/news/world/feed/");
					case 2: getContent("http://www.cnbc.com/id/100727362/device/rss/rss.html");
					case 3: getContent("http://mf.feeds.reuters.com/reuters/UKWorldNews");					
					case 4: getContent("http://news.sky.com/feeds/rss/world.xml");
					}
				}
			}
		}.start(); 
	}
	
	public void getContent(String rss) {
		final NewsContentExtractor newsExtract = new NewsContentExtractor();
		try {
			newsExtract.setURL(rss);
			newsExtract.setUpFeed();
			contentPane.setText("");
			contentPane.setText(newsExtract.getContent());
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}
}
