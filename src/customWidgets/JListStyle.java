package customWidgets;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Transparency;

import javax.swing.DefaultListCellRenderer;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;
import javax.swing.plaf.synth.SynthContext;
import javax.swing.plaf.synth.SynthPainter;

public class JListStyle {

	public static class CustomCellRenderer implements ListCellRenderer {
		
		@Override
		public Component getListCellRendererComponent(JList list, Object value,
				int index, boolean isSelected, boolean cellHasFocus) {
			ListItem item = (ListItem) value;
			JLabel cell = new JLabel(); //used to return this to the list
			cell.setPreferredSize(new Dimension(220,45));
			cell.setText(item.getName()); //set the text portion of the cell
			cell.setIcon(item.getIcon());
			cell.setFont(new Font("Arial",Font.BOLD,16));
			
			if (isSelected) {
				cell.setBackground(new Color(30,144,255,60));
				cell.setBorder(new LineBorder(new Color(30,144,255,50)));
			}
			else {
				cell.setBorder(new LineBorder(list.getBackground()));
				cell.setBackground(list.getBackground());
			}
			cell.setOpaque(true);
			return cell;			
		}
	}
}
