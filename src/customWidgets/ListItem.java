package customWidgets;

import java.awt.Image;

import javax.swing.ImageIcon;

public class ListItem {

	private Image image;	//image which will be used as icon in JList
	private String name;	//name for the item
	
	/**
	 * Method that sets the image for an JListItem
	 * @param image
	=========================================================*/
	public void setImage(Image image) {
		this.image=image;
	}
	
	/**
	 * Method that manipulates an image to desired scale
	 * @return an icon to be used in a JListItem
	=========================================================*/
	public ImageIcon getIcon() {
		ImageIcon icon = new ImageIcon(image.getScaledInstance
				(80, 40, Image.SCALE_FAST));
		return icon;
	}
	
	/**
	 * Set the name for the JListItem
	 * @param name
	=========================================================*/
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * Get the name of the JListItem
	 * @return
	=========================================================*/
	public String getName() {
		return name;
	}
}