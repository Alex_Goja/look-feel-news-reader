package news_content;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Iterator;
import java.util.List;

import javax.swing.JTextPane;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import com.sun.syndication.feed.synd.SyndEntry;
import com.sun.syndication.feed.synd.SyndFeed;
import com.sun.syndication.io.FeedException;
import com.sun.syndication.io.SyndFeedInput;
import com.sun.syndication.io.XmlReader;

public class NewsContentExtractor {
	
	private URL url;
	private HttpURLConnection httpConn;
	private SyndFeedInput syndInputFeed;
	private SyndFeed feed;
	private List entries;
	private String content = "";
	private JTextPane contentPane;
	
	public NewsContentExtractor() {}
	
	public void setURL(String urlString) throws MalformedURLException, IOException {
		try {
			url = new URL(urlString);
		}
		catch (MalformedURLException e) {
			System.err.println("URL not valid");
			e.printStackTrace();
		}
		catch (IOException e) {
			System.err.println("IOException in news extractor");
			e.printStackTrace();
		}
	}
	
	public void setUpFeed() throws IOException, IllegalArgumentException, FeedException {
		httpConn = (HttpURLConnection) url.openConnection();
		syndInputFeed = new SyndFeedInput();
		try {
			feed = syndInputFeed.build(new XmlReader(httpConn));
		}
		catch (IllegalArgumentException e) {
			System.err.println("IllegalArgument Exception - NewsContentExctractor");
		}
		entries = feed.getEntries();
		Iterator iteratorEntries = entries.iterator();
		while (iteratorEntries.hasNext()) {
			SyndEntry entry = (SyndEntry) iteratorEntries.next();
			content = content+ "\n" + entry.getTitle() + "\n";
			parseHTML(entry.getLink());		
		}
	}

	private void parseHTML(String url) {
		try {
			Document doc = Jsoup.connect(url).get();
			doc.select("p").last().remove();
			Element element = doc.select("p").first();
			while (element!=null) {
				content += element.text()+"\n";
				doc.select("p").first().remove();
				element = doc.select("p").first();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public String getContent() {
		return content;
	}
}

